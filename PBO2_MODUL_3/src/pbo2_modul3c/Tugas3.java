/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbo2_modul3c;

import java.awt.Container;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;

/**
 *
 * @author user
 */
public class Tugas3 extends JDialog {

    private final JLabel image;
    private final JLabel text;
    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 300;

    public Tugas3() {
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("Text and Icon Label");
        contentPane.setLayout(null);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        text = new JLabel("GRAPE");
        text.setBounds(35, 100, 97, 197);
        contentPane.add(text);
        ImageIcon imageic = new ImageIcon("Anggur.jpg");
        image = new JLabel();
        image.setIcon(imageic);
        image.setBounds(5, 10, 100, 100);
    }

    public static void main(String[] args) {
        Tugas3 dialog = new Tugas3();
        dialog.setVisible(true);
    }
}
