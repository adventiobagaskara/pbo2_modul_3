/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbo2_modul3c;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author user
 */
public class Tugas1 extends JFrame {

    private static final int FRAME_WIDTH = 350;
    private static final int FRAME_HEIGHT = 300;
    private static final int FRAME_X_ORIGIN = 200;
    private static final int FRAME_Y_ORIGIN = 300;
    private final JMenuBar menuBar;
    private final JMenu menu_File;
    private final JMenu menu_Edit;
    private final JMenuItem menuItemFile_tampil1;
    private final JMenuItem menuItemFile_tampil2;

    public static void main(String[] args) {
        Tugas1 frame = new Tugas1();
        frame.setVisible(true);
    }

    public Tugas1() {
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("Frame Pertama");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
        contentPane.setLayout(null);
        contentPane.setBackground(Color.pink);
        menuBar = new JMenuBar();
        menu_File = new JMenu("File");
        menu_Edit = new JMenu("Edit");
        menuBar.add(menu_File);
        menuBar.add(menu_Edit);
        this.setJMenuBar(menuBar);
        menuItemFile_tampil1 = new JMenuItem("Tampil 1");
        menuItemFile_tampil2 = new JMenuItem("Tampil 2");
        menu_File.add(menuItemFile_tampil1);
        menu_File.add(menuItemFile_tampil2);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
