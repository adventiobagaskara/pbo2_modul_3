/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbo2_modul3c;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JTextArea;

/**
 *
 * @author user
 */
public class Tugas4 extends JDialog {

    private final JButton left;
    private final JButton right;
    private final JCheckBox cen, bon, it;
    private final JTextArea text;
    private static final int FRAME_WIDTH = 600;
    private static final int FRAME_HEIGTH = 400;
    private static final int BUTTON_WIDTH = 90;
    private static final int BUTTON_HEIGTH = 40;

    public Tugas4() {
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGTH);
        setResizable(true);
        setTitle("CheckBoxDemo");
        contentPane.setLayout(null);
        contentPane.setBackground(Color.WHITE);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        text = new JTextArea("Welcome to Java");
        text.setBounds(170, 100, 140, 40);
        contentPane.add(text);
        left = new JButton("Left");
        left.setBounds(80, 200, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(left);
        right = new JButton("Right");
        right.setBounds(200, 200, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(right);
        cen = new JCheckBox("Centreed");
        cen.setBounds(300, 500, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(cen);
        bon = new JCheckBox("Bold");
        bon.setBounds(300, 75, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(bon);
        it = new JCheckBox("Italic");
        it.setBounds(300, 100, BUTTON_WIDTH, BUTTON_HEIGTH);
        contentPane.add(it);
    }

    public static void main(String[] args) {
        Tugas4 dialog = new Tugas4();
        dialog.setVisible(true);
    }
}
