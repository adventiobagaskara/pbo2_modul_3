/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbo2_modul3a;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author user
 */
public class FrameKu4 extends JFrame {

    public FrameKu4() {
        this.setSize(525, 200);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Find");
        this.setVisible(true);

        JPanel panel = new JPanel(null);
        JLabel label = new JLabel();
        JTextField teks = new JTextField();
        JButton tombol = new JButton();

        label.setText("Keyword : ");
        label.setBounds(225, 10, 100, 25);
        panel.add(label);
        this.add(panel);

        teks.setText("");
        teks.setBounds(100, 40, 300, 25);
        panel.add(teks);
        this.add(panel);

        tombol.setText("Find");
        tombol.setBounds(200, 80, 100, 25);
        panel.add(tombol);
        this.add(panel);
    }

    public static void main(String[] args) {
        new FrameKu4();
    }
}
