/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbo2_modul3a;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 *
 * @author user
 */
public class FrameKu3A extends JFrame {

    public FrameKu3A() {
        this.setSize(300, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini Class turunan dari class JFrame");
        this.setVisible(true);

        JPanel panel = new JPanel(null);
        JButton tombol = new JButton();
        JLabel label = new JLabel();
        JTextField teks = new JTextField();
        JCheckBox cek = new JCheckBox();
        JRadioButton radio = new JRadioButton();

        tombol.setText("Ini Tombol");
        tombol.setBounds(200, 10, 100, 30);
        panel.add(tombol);
        this.add(panel);

        label.setText("Ini Label");
        label.setBounds(200, 40, 140, 30);
        panel.add(label);
        this.add(panel);

        teks.setText("Ini Teks");
        teks.setBounds(200, 70, 180, 30);
        panel.add(teks);
        this.add(panel);

        cek.setText("Ini CheckBox");
        cek.setBounds(200, 100, 240, 30);
        panel.add(cek);
        this.add(panel);

        radio.setText("Ini Radio");
        radio.setBounds(200, 130, 280, 30);
        panel.add(radio);
        this.add(panel);
    }

    public static void main(String[] args) {
        new FrameKu3A();
    }
}
