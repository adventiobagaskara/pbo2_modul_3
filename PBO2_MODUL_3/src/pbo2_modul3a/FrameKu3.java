/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbo2_modul3a;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author user
 */
public class FrameKu3 extends JFrame {

    public FrameKu3() {
        this.setSize(300, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini Class turunan dari class JFrame");
        this.setVisible(true);

        JPanel panel = new JPanel();
        JButton tombol = new JButton();
        tombol.setText("Ini Tombol");
        panel.add(tombol);
        this.add(panel);
    }

    public static void main(String[] args) {
        new FrameKu3();
    }
}
